package com.ivroman.sorting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Arrays;

public class ArraySorter {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArraySorter.class);

    /**
     * The method that takes up to ten command-line arguments as integer values, sorts them in the ascending order, and then returns them as a String
     * @param strArray The array of strings to be sorted
     * @return The sorted array as a string
     */
    public String sort(String[] strArray) {
        LOGGER.info("Input array: {}", Arrays.toString(strArray));
        validate(strArray);
        int[] intArray = convertToIntArray(strArray);
        Arrays.sort(intArray);
        return convertToString(intArray);
    }

    private void validate(String[] strArray) {
        if (strArray == null) {
            throw new IllegalArgumentException("Input array is null");
        }
        if (strArray.length == 0) {
            throw new IllegalArgumentException("Input array is empty");
        }
        if (strArray.length > 10) {
            throw new IllegalArgumentException(String.format("Input array has %d elements (max 10)", strArray.length));
        }
        try {
            for (String s : strArray) {
                Integer.parseInt(s);
            }
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Input array includes non-integer element");
        }
    }

    private int[] convertToIntArray(String[] strArray) throws RuntimeException {
        int[] intArray = new int[strArray.length];
            for (int i = 0; i < strArray.length; i++) {
                intArray[i] = Integer.parseInt(strArray[i]);
            }
        return intArray;
    }

    private String convertToString(int[] sortedArray) {
        StringBuilder result = new StringBuilder();
        for (int i : sortedArray) {
            result.append(i).append(" ");
        }
        return result.toString().trim();
    }
}
