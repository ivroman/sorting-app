package com.ivroman.sorting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SortingApp {
    private static final Logger LOGGER = LoggerFactory.getLogger(SortingApp.class);

    /**
     * This is the start of the program
     * @param args The command line arguments
     */

    public static void main(String[] args) {
        try {
            LOGGER.info("Sorted array: {}", new ArraySorter().sort(args));
        } catch (IllegalArgumentException e) {
            LOGGER.error(e.getMessage());
        }
    }
}