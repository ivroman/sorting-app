package com.ivroman.sorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class ArraySorterParametrizedTest {
    String[] stringInput;
    String expected;
    public ArraySorterParametrizedTest(String[] stringInput, String expected) {
        this.stringInput = stringInput;
        this.expected = expected;
    }
    @Parameterized.Parameters
    public static Collection<Object> data() {
        Object[][] data = new Object[][] {
                {new String[] {"4", "3", "2", "1"}, "1 2 3 4"},
                {new String[] {"4", "4", "2", "1"}, "1 2 4 4"},
                {new String[] {"-4", "4", "2", "1"}, "-4 1 2 4"},
                {new String[] {"10", "9", "8", "7", "6", "5", "4", "3", "2", "1"}, "1 2 3 4 5 6 7 8 9 10"},
                {new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, "1 2 3 4 5 6 7 8 9 10"},
        };
        return Arrays.asList(data);
    }
    @Test
    public void testCorrectInput() {
        assertEquals(expected, new ArraySorter().sort(stringInput));
    }
}