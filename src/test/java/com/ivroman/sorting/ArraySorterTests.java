package com.ivroman.sorting;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ArraySorterTests {
    ArraySorter arraySorter = new ArraySorter();
    @Test
    public void testUnsortedArraysCase() {
        String[] array = {"0", "-1", "2"};
        String expected = "-1 0 2";
        assertEquals(expected, arraySorter.sort(array));
    }
    @Test
    public void testSortedArraysCase() {
        String[] array = {"-1", "0", "2"};
        String expected = "-1 0 2";
        assertEquals(expected, arraySorter.sort(array));
    }
    @Test
    public void testSingleElementArrayCase() {
        String[] array = {"2"};
        String expected = "2";
        assertEquals(expected, arraySorter.sort(array));
    }
    @Test
    public void testTenElementsArrayCase() {
        String[] array = {"10", "9", "8", "7", "6", "5", "4", "3", "2", "1"};
        String expected = "1 2 3 4 5 6 7 8 9 10";
        assertEquals(expected, arraySorter.sort(array));
    }
    @Test (expected = IllegalArgumentException.class)
    public void testElevenElementsArrayCase() {
        String[] array = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"};
        arraySorter.sort(array);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testNullCase() {
        String[] array = null;
        arraySorter.sort(array);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testEmptyCase() {
        String[] array = {""};
        arraySorter.sort(array);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testStringCase() {
        String[] array = {"s"};
        arraySorter.sort(array);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testDoubleCase() {
        String[] array = {"1.25"};
        arraySorter.sort(array);
    }
}